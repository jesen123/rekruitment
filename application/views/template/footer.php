		<script src="<?=base_url('/asset/metronic')?>/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/assets/app/js/dashboard.js" type="text/javascript"></script>
		<script>
			$(document).ready(function () {
				$("#Daftar").hide();
				$("#educationData").hide();
				$("#experienceData").hide();
				$("#skillData").hide();
				$("#summaryData").hide();
			});

			$(window).on('load', function() {
				$('body').removeClass('m-page--loading');
			});

			$(window).scroll(function() {
				if ($(this).scrollTop() > 50) {
					console.log($(this).scrollTop())
					$('#cardKriteria').addClass('card-kriteria')
					$("#cardKriteria").addClass('card-fixed')
				} else {
					$('#cardKriteria').removeClass('card-kriteria')
					$('#cardKriteria').removeClass('card-fixed')
				}
			});

			function personalData(){
				$("#personalData").hide();
				$("#educationData").show();
				$("#pageInfo").html("Detail Personal  > Education ");
			}

			function educationData(){
				$("#educationData").hide();
				$("#experienceData").show();
				$("#pageInfo").html("Detail Personal  > Education > Experience ");
			}

			function experienceData(){
				$("#experienceData").hide();
				$("#skillData").show();
				$("#pageInfo").html("Detail Personal  > Education > Experience > Skill ");
			}

			function skillData(){
				$("#skillData").hide();
				$("#summaryData").show();
				$("#pageInfo").html("Detail Personal  > Education > Experience > Skill > Summary About You");
			}

			function updateType(type){
				$("#type").hide()
				$("#formDaftar").fadeIn();
				$("#typeLogin").value(type)
			}

			function sliderCard(){
				$("#type").show()
				$("#formDaftar").hide();
				$('.card-daftar').removeClass('cardSlide');
				$('.card-login').addClass('cardSlide');
				setTimeout(() => {
					$("#login").fadeOut();
					$("#Daftar").fadeIn(1000);
				}, 1000);
			}

			function sliderCardDaftar(){
				$('.card-login').removeClass('cardSlide');
				$('.card-daftar').addClass('cardSlide');
				setTimeout(() => {
					$("#Daftar").fadeOut();
					$("#login").fadeIn(1000);
				}, 1000);
			}
		</script>
	</body>
</html>